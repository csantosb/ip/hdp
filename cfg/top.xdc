# * Clk

set_property IOSTANDARD LVCMOS33 [get_ports clk]
set_property PACKAGE_PIN F17 [get_ports clk]
create_clock -period 10.000 -name clk_time [get_ports clk]

# * Reset

set_property IOSTANDARD LVCMOS33 [get_ports rst]
set_property PACKAGE_PIN D16 [get_ports rst]

# * Input

set_property IOSTANDARD LVCMOS18 [get_ports {input[0]}]
set_property PACKAGE_PIN U6 [get_ports {input[0]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[0]]

set_property IOSTANDARD LVCMOS18 [get_ports {input[1]}]
set_property PACKAGE_PIN U5 [get_ports {input[1]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[1]]

set_property IOSTANDARD LVCMOS18 [get_ports {input[2]}]
set_property PACKAGE_PIN U2 [get_ports {input[2]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[2]]

set_property IOSTANDARD LVCMOS18 [get_ports {input[3]}]
set_property PACKAGE_PIN U1 [get_ports {input[3]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[3]]

set_property IOSTANDARD LVCMOS18 [get_ports {input[4]}]
set_property PACKAGE_PIN W6 [get_ports {input[4]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[4]]

set_property IOSTANDARD LVCMOS18 [get_ports {input[5]}]
set_property PACKAGE_PIN W5 [get_ports {input[5]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[5]]

set_property IOSTANDARD LVCMOS18 [get_ports {input[6]}]
set_property PACKAGE_PIN V3 [get_ports {input[6]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[6]]

set_property IOSTANDARD LVCMOS18 [get_ports {input[7]}]
set_property PACKAGE_PIN W3 [get_ports {input[7]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[7]]

set_property IOSTANDARD LVCMOS18 [get_ports {input[8]}]
set_property PACKAGE_PIN U7 [get_ports {input[8]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[8]]

set_property IOSTANDARD LVCMOS18 [get_ports {input[9]}]
set_property PACKAGE_PIN V6 [get_ports {input[9]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[9]]

set_property IOSTANDARD LVCMOS18 [get_ports {input[10]}]
set_property PACKAGE_PIN V4 [get_ports {input[10]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[10]]

set_property IOSTANDARD LVCMOS18 [get_ports {input[11]}]
set_property PACKAGE_PIN W4 [get_ports {input[11]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[11]]

# * Output

set_property IOSTANDARD LVCMOS18 [get_ports {output[0]}]
set_property PACKAGE_PIN A10 [get_ports {output[0]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[32]]

set_property IOSTANDARD LVCMOS18 [get_ports {output[1]}]
set_property PACKAGE_PIN B10 [get_ports {output[1]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[33]]

set_property IOSTANDARD LVCMOS18 [get_ports {output[2]}]
set_property PACKAGE_PIN D10 [get_ports {output[2]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[34]]

set_property IOSTANDARD LVCMOS18 [get_ports {output[3]}]
set_property PACKAGE_PIN E10 [get_ports {output[3]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[35]]

set_property IOSTANDARD LVCMOS18 [get_ports {output[4]}]
set_property PACKAGE_PIN B9 [get_ports {output[4]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[36]]

set_property IOSTANDARD LVCMOS18 [get_ports {output[5]}]
set_property PACKAGE_PIN C9 [get_ports {output[5]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[37]]

set_property IOSTANDARD LVCMOS18 [get_ports {output[6]}]
set_property PACKAGE_PIN A8 [get_ports {output[6]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[38]]

set_property IOSTANDARD LVCMOS18 [get_ports {output[7]}]
set_property PACKAGE_PIN A9 [get_ports {output[7]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[39]]

set_property IOSTANDARD LVCMOS18 [get_ports {output[8]}]
set_property PACKAGE_PIN D8 [get_ports {output[8]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[40]]

set_property IOSTANDARD LVCMOS18 [get_ports {output[9]}]
set_property PACKAGE_PIN D9 [get_ports {output[9]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[41]]

set_property IOSTANDARD LVCMOS18 [get_ports {output[10]}]
set_property PACKAGE_PIN B11 [get_ports {output[10]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[42]]

set_property IOSTANDARD LVCMOS18 [get_ports {output[11]}]
set_property PACKAGE_PIN B12 [get_ports {output[11]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[43]]

set_property IOSTANDARD LVCMOS18 [get_ports {output[12]}]
set_property PACKAGE_PIN C12 [get_ports {output[12]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[44]]
