# * About

# This makefile is intended to be used as a common base for ghdl and cocotb simulations.

# It may be found at : gitlab.com/csantosb/eda-common/common/Makefile.edacommon

# * Use

# For its use, refer to readme.org and follow below indications. Note that an
# example use of each variable is provided too. Finally, if a variable is left
# empty, a default value is taken.

# * Project variables

# Put your own variables in here
# PRJ_VAR = 1

# * Generics

# Number of input streams
# g_generic1 =

# data bus width
# g_generic2 =

# * Common variables

# Mandatory variable
# Workflow: one of ghdl / cocotb
# EDAMODE = ghdl
EDAMODE = ghdl

# Flag for debugging
# DEBUG = 1
export DEBUG ?=

# Make compilation verbose setting this variable to anything but empty
ifneq ($(DEBUG),)
SHOWINFOS = 1
endif

# ** Sources

# *** Under current project

# PROJECT_ROOTDIR is deduced from git root dir

# Directories where project sources are to be found
# PRJ_SRC_DIR = {sim/runs/mux_fifo.cocotb/hdl,src/ip}
# PRJ_SRC_DIR = src
PRJ_SRC_DIR := {sim/runs/hpd.ghdl/hdl,src}

# Sparse source rtl code
# PRJ_SRC_FILES = {ip/mux_fifo_fifo/mux_fifo_fifo_sim_netlist.vhdl,ip/mux_fifo_fifo_out/mux_fifo_fifo_out_sim_netlist.vhdl}
PRJ_SRC_FILES =

# *** Not under current project

# Directory with ancillary sources
# NO_PROJECT_ROOTDIR = /tmp/build/mux_fifo_0/src/mux_fifo_0
NO_PROJECT_ROOTDIR =

# Directories where non project sources are to be found
# NO_PRJ_SRC_DIR = {src,sim/runs/mux_fifo.cocotb/hdl}
# NO_PRJ_SRC_DIR = src
NO_PRJ_SRC_DIR =

# Sparse source rtl code
# NO_PRJ_SRC_FILES = {ip/mux_fifo_fifo/mux_fifo_fifo_sim_netlist.vhdl,ip/mux_fifo_fifo_out/mux_fifo_fifo_out_sim_netlist.vhdl}
NO_PRJ_SRC_FILES =

# ** Ghdl

# To be used with ghdl

# Kind of wave file to save results to: one of ghw/vcd
# When left empty, no wave is recorded
# GHDL_WAVE_TYPE = ghw
# ifneq ($(DEBUG),)
# GHDL_WAVE_TYPE = ghw
# endif
GHDL_WAVE_TYPE = ghw

# Duration of simulation
# GHDL_SIM_TIME = 200us
GHDL_SIM_TIME = 950us

# Topmost level module name
# GHDL_TOPLEVEL = top
GHDL_TOPLEVEL = hpd_tb

# Xilinx libraries to be used: one of Vivado / ISE
# GHDL_XILINX_TOOL = Vivado
GHDL_XILINX_TOOL = Vivado

# VHDL standard: one of 08 / 93c
# GHDL_STD = 08
GHDL_STD = 08

 # IEEE libraries : one of synopsys / standard
# GHDL_IEEE = standard
GHDL_IEEE = standard

# Simulation flags
# GHDL_RUNFLAGS = --ieee-asserts=enable -gg_generic1=$(g_generic1) -gg_generic2=$(g_generic2)
GHDL_RUNFLAGS = --ieee-asserts=enable

# ** Cocotb

# Applies when simulating with cocotb

# Remembar that as for [[https://stackoverflow.com/questions/47475325/pass-argument-from-makefile-to-cocotb-testbench][this]] and [[https://stackoverflow.com/questions/23843106/how-to-set-child-process-environment-variable-in-makefile][that]], any variable in the make file may be made accessible to the
# python testbench by using

# export VARIABLENAME =

# and then using import os: os.environ[’VARIABLENAME']

# TODO: rename to COCOTB_MODULE and convert it back in its file
# Python testbench file name
# MODULE := p1_counter_tb
MODULE =

# * Include global makefile

# Include the generic makefile, common to ghdl / cocotb flows

# To be found at : gitlab.com/csantosb/eda-common/common/Makefile.edacommon.generic

ifeq ($(EDA_COMMON_DIR),)
$(info  "info: EDA_COMMON_DIR is empty")
$(info  "      Please declare it as an env variable")
$(info  "      or declare it as")
$(info  "          make EDA_COMMON_DIR=/your_path/")
$(info )
$(error "EDA_COMMON_DIR empty")
endif

ifneq ($(SHOWINFOS),)
$(info )$(info "info: EDA_COMMON_DIR is $(EDA_COMMON_DIR)")$(info )
endif

ifneq ($(SHOWINFOS),)
$(info "info: Including $(EDA_COMMON_DIR)/common/Makefile.edacommon.generic")$(info )
endif
-include $(EDA_COMMON_DIR)/common/Makefile.edacommon.generic

# * Include specific makefile following the workflow in use

ifeq ($(EDAMODE),)
$(error "EDAMODE empty")
endif

# ghdl case
ifeq ($(EDAMODE),ghdl)
ifneq ($(SHOWINFOS),)
$(info "info: Including $(EDA_COMMON_DIR)/ghdl/Makefile.edacommon.ghdl")
endif
-include $(EDA_COMMON_DIR)/ghdl/Makefile.edacommon.ghdl
endif

# cocotb case
ifeq ($(EDAMODE),cocotb)
ifneq ($(SHOWINFOS),)
$(info "info: Including $(EDA_COMMON_DIR)/cocotb/Makefile.edacommon.cocotb")
endif
-include $(EDA_COMMON_DIR)/cocotb/Makefile.edacommon.cocotb
endif
